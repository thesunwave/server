require 'rack'
require 'active_support/time'
require 'uri'

CONTENT_TYPE = {'Content-Type' => 'text/html'}.freeze

app = Proc.new do |env|
  router(env['REQUEST_URI'])
end

def router(path)
  response(URI(path).path =~ /time/ ? :success_route : :error_route, path)
end

def response(type, args)
  return ['404', CONTENT_TYPE, ["#{args} is a wrong path"]] if type == :error_route
  ['200', CONTENT_TYPE, [get_time(args)]] if type == :success_route
end

def get_time(params)
  cities = URI(params).query&.split(',')
  result = [
      time_formatter('UTC'),
      cities&.map do |city|
        time_formatter(URI::decode(city).strip)
      end
  ]

  result.join('<br/>')
end

def time_formatter(city)
  begin
    Time.zone = city
    [city, Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')].join(': ')
  rescue ArgumentError
    [city, 'Timezone is not present'].join(': ')
  end
end

Rack::Handler::WEBrick.run app
